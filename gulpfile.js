const autoprefixer = require('gulp-autoprefixer');
const babel        = require('gulp-babel');
const concat       = require('gulp-concat');
const environments = require('gulp-environments');
const gulp         = require('gulp');
const sass         = require('gulp-sass');
const sourcemaps   = require('gulp-sourcemaps');
const uglify     = require('gulp-uglify');

const development = environments.development;
const production  = environments.production;

const sassOptions = {includePaths: ['node_modules/foundation-sites/scss']};

sassOptions['outputStyle'] = production() ? "compressed" : "expanded";

gulp.task('sass', function() {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(development(sourcemaps.init()))
        .pipe(sass(sassOptions))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9', 'Android >= 2.3', 'ios >= 7'],
            cascade: false
        }))
        .pipe(development(sourcemaps.write('.')))
        .pipe(gulp.dest('./_site/css'));
});

gulp.task('scripts', function() {
    return gulp.src(['./node_modules/jquery/dist/jquery.js', './node_modules/foundation-sites/dist/js/foundation.js'])
        .pipe(development(sourcemaps.init()))
        .pipe(babel())
        .pipe(concat('app.js'))
        .pipe(development(sourcemaps.write('.')))
        .pipe(production(uglify()))
        .pipe(gulp.dest('./_site/js'))
});

gulp.task('default', ['sass', 'scripts']);